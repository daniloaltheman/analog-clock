//
//  BattView.swift
//  Batt Loading
//
//  Created by Danilo Altheman on 02/07/14.
//  Copyright (c) 2014 Quaddro (http://www.quaddro.com.br). All rights reserved.
//

import UIKit

class ClockView: UIView {
    init(coder aDecoder: NSCoder!) {
        super.init(coder: aDecoder)
        self.start()
    }
    
    init(frame: CGRect) {
        super.init(frame: frame)
        self.start()
    }
    
    func start() {
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
    }
    
    func update() {
        self.setNeedsDisplay()
    }
    
    func degreesToRadians(degrees: Double) -> Double {
        return degrees / 180 * Double(M_PI)
    }
    
    override func drawRect(rect: CGRect)  {

        let midX = CGRectGetMidX(self.frame)
        let midY = CGRectGetMidY(self.frame)
        let screenCenter = CGPointMake(midX, midY)
        
        var clockRadius: CGFloat = 200
        var pointerScale = clockRadius / 8.0
        
        var minuteLineLenght: CGFloat = 230.0
        var secondLineLenght: CGFloat = 230.0
        var hourLineLenght: CGFloat = 260.0

        var dateComponents: NSDateComponents = NSCalendar.currentCalendar().components(NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute | NSCalendarUnit.CalendarUnitSecond, fromDate: NSDate())

        var hour = Double(dateComponents.hour)
        if hour > 12 { hour -= 12 }
        var minutes = Double(dateComponents.minute)
        var seconds = Double(dateComponents.second)
        
        var hourHand = UIBezierPath()
        hourHand.moveToPoint(screenCenter)
        var hourHandPosX = midX + CGFloat(clockRadius - hourLineLenght / 2) * CGFloat(sin(2 * M_PI / 12 * hour))
        var hourHandPosY = midY - CGFloat(clockRadius - hourLineLenght / 2) * CGFloat(cos(2 * M_PI / 12 * hour))
        hourHand.addLineToPoint(CGPointMake(hourHandPosX, hourHandPosY))
        UIColor.whiteColor().set()
        hourHand.lineWidth = pointerScale * 0.4
        hourHand.lineCapStyle = kCGLineCapRound
        hourHand.stroke()
        
        var minuteHand = UIBezierPath()
        minuteHand.moveToPoint(screenCenter)
        var minuteHandPosX = midX + CGFloat(clockRadius - minuteLineLenght / 2) * CGFloat(sin(2 * M_PI / 60 * minutes))
        var minuteHandPosY = midY - CGFloat(clockRadius - minuteLineLenght / 2) * CGFloat(cos(2 * M_PI / 60 * minutes))
        minuteHand.addLineToPoint(CGPointMake(minuteHandPosX, minuteHandPosY))
        UIColor.whiteColor().set()
        minuteHand.lineWidth = pointerScale * 0.4
        minuteHand.lineCapStyle = kCGLineCapRound
        minuteHand.stroke()
        
        var secondHand = UIBezierPath()
        secondHand.moveToPoint(screenCenter)
        var secondHandPosX = midX + CGFloat(clockRadius - secondLineLenght / 2) * CGFloat(sin(2 * M_PI / 60 * seconds))
        var secondHandPosY = midY - CGFloat(clockRadius - secondLineLenght / 2) * CGFloat(cos(2 * M_PI / 60 * seconds))
        secondHand.addLineToPoint(CGPointMake(secondHandPosX, secondHandPosY))
        UIColor.redColor().set()
        secondHand.lineWidth = pointerScale * 0.15
        secondHand.lineCapStyle = kCGLineCapRound
        secondHand.stroke()
       
        var clockFace = UIBezierPath(ovalInRect: CGRectMake(midX - (clockRadius / 2), midY - (clockRadius / 2), clockRadius, clockRadius))
        UIColor.whiteColor().set()
        clockFace.lineWidth = pointerScale * 0.6
        clockFace.stroke()
    }
}
