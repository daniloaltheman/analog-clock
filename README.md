# An Analog Clock written in Swift. #

## The AnalogClock Class uses sin, cos and UIBezierPath to draw the clock.

### http://swiftplayground.com.br/swift-analog-clock-view/

![alt Screenshot](http://i.imgur.com/SPJSj73.png)